# BigBagKbdTrixKmonad

"DreymaR's Big Bag of Keyboard Tricks" for kmonad

## Getting started

Just select the version you want and change the device on the `defsrc` block to your preferred keyboard, then run it with Kmonad.
